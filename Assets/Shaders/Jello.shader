﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Tams/Jello"
{
	Properties 
	{
		_Color ("Tint", Color) = (0, 0, 0, 1)
		_MainTex ("Texture", 2D) = "white" {}
		[HDR] _Emission ("Emission", color) = (0,0,0)

		_Ramp("Toon Ramp", 2D) = "white" {}

		_JiggleMultiplier("Multiplier of the Jiggles", Range(0.01, 1)) = 1
		_DistortionMultiplier("Distortion of the top of the jelly", Float) = 1

		_Velocity("Velocity", Vector) = (1, 1, 0, 0)
		_OriginOfJiggle("Origin of jiggle", Vector) = (0, 0, 0, 1)

	}
	SubShader {

		Tags{ "RenderType"="Opaque" "Queue"="Geometry"}

		CGPROGRAM

		#pragma surface surf Custom fullforwardshadows vertex:vert addshadow
		#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
		sampler2D _Ramp;

		half3 _Emission;

		float _JiggleMultiplier;
		float _DistortionMultiplier;

		float4 _Velocity;
		float4 _OriginOfJiggle;

		struct Input 
		{
			float2 uv_MainTex;
		};

		void vert(inout appdata_full data)
		{
			float4 initialVertexPosition = data.vertex;
			
			float4 dist = distance(data.vertex, float4(0.0, 0.1, 0.0, 0.0));

			//float4 dist = mul(unity_ObjectToWorld, dist1);

			float4 currentVertexPosition = initialVertexPosition - data.vertex;

			float asd = length(_Velocity);

			// THIS
			//float4 positionToBeIn = data.vertex + (_Velocity * dist) - currentVertexPosition;
			//float4 positionToBeIn = data.vertex + (_Velocity * dist) - currentVertexPosition;

			
			//float4 clampedPosition = clamp(currentVertexPosition, positionToBeIn, 1);

			//THIS
			//data.vertex = lerp(initialVertexPosition, positionToBeIn, distance(initialVertexPosition, mul(unity_ObjectToWorld, _OriginOfJiggle)));

			// UUSI YRITYS ÄÄÄÄ

			float4 vertexPosition = data.vertex;
	
			vertexPosition = vertexPosition + _Velocity * dist;

			float heightMultiplier = mul(unity_ObjectToWorld, vertexPosition.y);

			data.vertex = lerp(data.vertex, vertexPosition, heightMultiplier * -_Velocity * _JiggleMultiplier);


			//data.vertex = lerp(data.vertex, vertexPosition, vertexPosition.y * -_Velocity * _JiggleMultiplier);
			data.vertex = lerp(data.vertex, vertexPosition, heightMultiplier * -_Velocity * _JiggleMultiplier);


			//data.vertex = positionToBeIn;
			//data.vertex = data.vertex + (_Velocity * dist) - currentVertexPosition;

			//float4 currentVertexPosition = initialVertexPosition - data.vertex;
		}

		float4 LightingCustom(SurfaceOutput s, float3 lightDir, float atten)
		{
			float towardsLight = dot(s.Normal, lightDir);
			towardsLight = towardsLight * 0.5 + 0.5;

			float3 lightIntensity = tex2D(_Ramp, towardsLight).rgb;

			float4 col;

			col.rgb = lightIntensity *s.Albedo * atten * _LightColor0.rgb;

			col.a = s.Alpha;

			return col;
		}

		void surf (Input i, inout SurfaceOutput o) {
			fixed4 col = tex2D(_MainTex, i.uv_MainTex);
			col *= _Color;
			o.Albedo = col.rgb;
			
			//o.Emission = _Emission;
		}
		ENDCG
	}
	FallBack "Standard"
}
