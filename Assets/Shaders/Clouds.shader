﻿Shader "Tams/Clouds"
{
	Properties 
	{
		_Color ("Tint", Color) = (0, 0, 0, 1)
		_MainTex ("Texture", 2D) = "white" {}
		[HDR] _Emission ("Emission", color) = (0,0,0)
		[PowerSlider(4)] _FresnelExponent("Fresnel Exponent", Range(0.25, 4)) = 1

		_Ramp("Toon Ramp", 2D) = "white" {}
	}
	SubShader {

		Tags{ "RenderType"="Transparent" "Queue"="Transparent"}

		Cull Back
		ZWrite On

		CGPROGRAM

		#pragma surface surf Custom fullforwardshadows alpha
		#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
		sampler2D _Ramp;

		half3 _Emission;
		float _FresnelExponent;

		struct Input 
		{
			float2 uv_MainTex;
			float3 worldNormal;
			float3 viewDir;
			INTERNAL_DATA
		};

		float4 LightingCustom(SurfaceOutput s, float3 lightDir, float atten)
		{
			float towardsLight = dot(s.Normal, lightDir);
			towardsLight = towardsLight * 0.5 + 0.5;

			float3 lightIntensity = tex2D(_Ramp, towardsLight).rgb;

			float4 col;

			col.rgb = lightIntensity *s.Albedo * atten * _LightColor0.rgb;

			col.a = s.Alpha;

			return col;
		}

		void surf (Input i, inout SurfaceOutput o) {
			fixed4 col = tex2D(_MainTex, i.uv_MainTex);
			//col *= _Color;			
			
			float fresnel = dot(i.worldNormal, i.viewDir);

			// Outside to Inside
			//fresnel = saturate(1 - fresnel);

			// Inside to Outside (used for the clouds)
			fresnel = saturate(fresnel);

			fresnel = pow(fresnel, _FresnelExponent);

			col.a *= fresnel * _Color.a;

			//o.Emission = _Emission + fresnel;

			o.Albedo = col.rgb;
			o.Alpha = col.a;
		}
		ENDCG
	}
	FallBack "Standard"
}
