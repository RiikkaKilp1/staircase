﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Tams/MaunoJello"
{
	Properties 
	{
		_Color ("Tint", Color) = (0, 0, 0, 1)
		_MainTex ("Texture", 2D) = "white" {}
		[HDR] _Emission ("Emission", color) = (0,0,0)

		_Ramp("Toon Ramp", 2D) = "white" {}

		_JiggleLimit("Jiggle Limit", Range(0, 1)) = 1
		_DistortionMultiplier("Distortion of the top of the jelly", Float) = 1

		_Velocity("Velocity", Vector) = (1, 1, 0, 0)
		_OriginOfJiggle("Origin of jiggle", Vector) = (0, 0, 0, 1)

	}
	SubShader {

		Tags{ "RenderType"="Opaque" "Queue"="Geometry"}

		CGPROGRAM

		#pragma surface surf Custom fullforwardshadows vertex:vert addshadow
		#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
		sampler2D _Ramp;

		half3 _Emission;

		float _JiggleLimit;
		float _DistortionMultiplier;

		float4 _Velocity;
		float4 _OriginOfJiggle;

		struct Input 
		{
			float2 uv_MainTex;
		};

		void vert(inout appdata_full data)
		{
			float velocityX = _Velocity.x;
			float velocityZ = _Velocity.z;

			float offsetX = _JiggleLimit * clamp(velocityX, -1, 1);
			float offsetZ = _JiggleLimit * clamp(velocityZ, -1, 1);

			//float4 newPos = _OriginOfJiggle + float4(0, 1, 0, 0) * data.vertex.y;

			//float4 newPos = float4(0, 1, 0, 0) * data.vertex.y;

			// Riikkastuff

			float4 modifiedPosition = data.vertex;
			modifiedPosition.x -= offsetX * data.vertex.y;
			modifiedPosition.z -= offsetZ * data.vertex.y;

			data.vertex = modifiedPosition;

			//data.vertex.x += -offsetX * data.vertex.y;
			//data.vertex.z += -offsetZ * data.vertex.y;


			//riikkastuff end

			//newPos += offsetX * data.vertex.y + offsetZ * data.vertex.y;

			//data.vertex.x = newPos.x;
			//data.vertex.z = newPos.z;
		}

		float4 LightingCustom(SurfaceOutput s, float3 lightDir, float atten)
		{
			float towardsLight = dot(s.Normal, lightDir);
			towardsLight = towardsLight * 0.5 + 0.5;

			float3 lightIntensity = tex2D(_Ramp, towardsLight).rgb;

			float4 col;

			col.rgb = lightIntensity *s.Albedo * atten * _LightColor0.rgb;

			col.a = s.Alpha;

			return col;
		}

		void surf (Input i, inout SurfaceOutput o) {
			fixed4 col = tex2D(_MainTex, i.uv_MainTex);
			col *= _Color;
			o.Albedo = col.rgb;
			
			//o.Emission = _Emission;
		}
		ENDCG
	}
	FallBack "Standard"
}
