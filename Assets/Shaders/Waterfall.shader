﻿Shader "Tams/Waterfall"
{
	Properties 
	{
		_Color ("Tint", Color) = (0, 0, 0, 1)
		_MainTex ("Scroll Texture", 2D) = "white" {}

		_Ramp("Toon Ramp", 2D) = "white" {}

		//Scroll
		_ScrollSpeed("UV Scroll Speed", Float) = 1
		_ScrollDirection("Scroll Direction", Vector) = (1, 1, 0, 0)
		_Cutoff("Alpha Cutoff", Range(0, 1)) = 0.5

		//Popping
        _Amplitude("Amplitude", Float) = 1
        _Speed ("Speed", Float) = 1
	}
	SubShader {

		Tags{ "RenderType"="Transparent" "Queue"="Transparent"}

		Cull Off
		ZWrite On

		CGPROGRAM

		//#pragma surface surf Custom fullforwardshadows vertex:vert
		#pragma surface surf Custom fullforwardshadows alpha vertex:vert
		#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;

		sampler2D _Ramp;

		float _ScrollSpeed;
		float2 _ScrollDirection;
		float _Cutoff;

		float _Speed;
		float _Amplitude;

		struct Input 
		{
			float2 uv_MainTex;
		};
		

        void vert (inout appdata_full v)
        {
			float3 offset = float3(0, 0, 0);
			offset.x += _Amplitude * sin(_Time.y * _Speed);
			v.vertex.xyz += offset;
        }
		
		float4 LightingCustom(SurfaceOutput s, float3 lightDir, float atten)
		{
			float towardsLight = dot(s.Normal, lightDir);
			towardsLight = towardsLight * 0.5 + 0.5;

			float3 lightIntensity = tex2D(_Ramp, towardsLight).rgb;

			float4 col;

			col.rgb = lightIntensity *s.Albedo * atten * _LightColor0.rgb;

			col.a = s.Alpha;

			return col;
		}
		
		void surf (Input i, inout SurfaceOutput o) 
		{
			float2 scrolledUV = i.uv_MainTex;
			scrolledUV += float2(_ScrollDirection.x, _ScrollDirection.y) * _Time.y * _ScrollSpeed;
			
			fixed4 col = tex2D(_MainTex, scrolledUV);
			//o.Alpha = col.b - 0.5;

			float ca = tex2D(_MainTex, scrolledUV).r;

			if (ca > _Cutoff)
				o.Alpha = col.a;
			else
				o.Alpha = 0;

			//col = round(col * 5.0) / 5.0;

			col *= _Color;
						
			o.Albedo = col.rgb;		
		}
		ENDCG
	}
	FallBack "Standard"
}
