﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jellyfier : MonoBehaviour
{
    public float bounceSpeed;
    public float fallForce;
    public float stiffness;
    public float pokeForce;
    public float velocitySpeed;

    public GameObject plate;
    public float jiggleForce;
    public float jiggleExponent;

    public bool pressureFromVelocity = true;

    private Vector3 externalVelocity;

    private MeshFilter meshFilter;
    private Mesh mesh;

    JellyVertex[] jellyVertices;
    Vector3[] currentMeshVertices;

    public Rigidbody rb;

    private void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        mesh = meshFilter.mesh;

        GetVertices();
    }

    private void GetVertices()
    {
        jellyVertices = new JellyVertex[mesh.vertices.Length];
        currentMeshVertices = new Vector3[mesh.vertices.Length];

        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            jellyVertices[i] = new JellyVertex(i, mesh.vertices[i], mesh.vertices[i], Vector3.zero);
            currentMeshVertices[i] = mesh.vertices[i];
        }
    }

    private void Update()
    {

        UpdateVertices();

        if (Input.GetButton("Fire1"))
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.rigidbody != null)
                {
                    ApplyPressureToPoint(hit.point, pokeForce);
                }
            }
        }

        if (pressureFromVelocity)
        {
            externalVelocity = rb.velocity;
        }


        else
            externalVelocity = Vector3.zero;
    }

    private void UpdateVertices()
    {
        for(int i = 0; i < jellyVertices.Length; i++)
        {
            float distance = Vector3.Distance(plate.transform.position, jellyVertices[i].currentVertexPosition);

            jellyVertices[i].UpdateVelocity(bounceSpeed, externalVelocity, distance * 10f);
            jellyVertices[i].Settle(stiffness);

            jellyVertices[i].currentVertexPosition += jellyVertices[i].currentVelocity * Time.deltaTime;
            currentMeshVertices[i] = jellyVertices[i].currentVertexPosition;
        }

        mesh.vertices = currentMeshVertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }

    public void OnCollisionEnter(Collision other)
    {
        ContactPoint[] collisionPoints = other.contacts;
        for (int i = 0; i < collisionPoints.Length; i++)
        {
            Vector3 inputPoint = collisionPoints[i].point + (collisionPoints[i].point * 1f);
            ApplyPressureToPoint(inputPoint, fallForce * other.rigidbody.mass);
        }
    }

    public void ApplyPressureToPoint(Vector3 _point, float _pressure)
    {
        for (int i = 0; i < jellyVertices.Length; i++)
        {
            jellyVertices[i].ApplyPressureToVertex(transform, _point, _pressure);
        }
    }
}
