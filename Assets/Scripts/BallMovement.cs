﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour
{

    private Rigidbody rb;
    public float Speed = 10;
    public float LookSpeed = 1;

    public GameObject Car;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        rb.AddForce(new Vector3 (Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * Speed, ForceMode.Acceleration);
        Car.transform.position = transform.position;
        Car.transform.rotation = Quaternion.Lerp(Car.transform.rotation, Quaternion.LookRotation(rb.velocity), LookSpeed);
        //Car.transform.rotation = Quaternion.Lerp(Car.transform.rotation, Quaternion.LookRotation(Car.transform.position), Time.fixedDeltaTime * LookSpeed);
    }
}
