﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLookAt : MonoBehaviour
{

    public GameObject CameraToFollow;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(CameraToFollow.transform);
    }
}
