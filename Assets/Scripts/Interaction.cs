﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{

    public GameObject InteractionBubble;

    public float BubbleMinSize;
    public float BubbleMaxSize;

    public float MinWaitTime;
    public float MaxWaitTime;

    private float startTime;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            startTime = Time.time;
        }

        if (Input.GetKeyUp(KeyCode.Space) && Time.time >= startTime)
        {
            float lenghtOfInput = Time.time - startTime;
            GameObject newBubble = Instantiate(InteractionBubble);

            float clampedTime = Mathf.InverseLerp(MinWaitTime, MaxWaitTime, lenghtOfInput);
            float sizeScale = Mathf.Lerp(BubbleMinSize, BubbleMaxSize, clampedTime);

            newBubble.transform.localScale = new Vector3(sizeScale, sizeScale, sizeScale);
        }
    }
}
