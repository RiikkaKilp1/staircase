﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellyfierViaShader : MonoBehaviour
{
    public Rigidbody rb;
    public Vector3 velocity;
    private Material mat;
    private MeshRenderer meshRend;

    public GameObject plate;
    private Vector3 originOfJiggle;
    private void Start()
    {
        //meshRend = GetComponent<MeshRenderer>();
        mat = GetComponent<Renderer>().material;
        originOfJiggle = plate.transform.position;
    }

    public void Update()
    {
        mat.SetVector("_Velocity", velocity);
        mat.SetVector("_OriginOfJiggle", originOfJiggle);
        velocity = rb.velocity;
    }
}
